using OQ.MineBot.PluginBase.Classes;
using OQ.MineBot.Protocols.Classes.Base;
using System;

namespace OQ.VectorHelpers
{
    public class Vector3I
    {
        public int X;
        public int Y;
        public int Z;
        public Vector3I(int x = 0, int y = 0, int z = 0)
        {
            X = x;
            Y = y;
            Z = z;
        }
        public IPosition ToPosition()
            => new Position(X, Y, Z);
        public ILocation ToLocation()
            => new Location(X,Y,Z);
        public static Vector3I FromLocation(ILocation a)
            => new Vector3I(a.X,(int)Math.Round(a.Y), a.Z);
        public static Vector3I FromPosition(IPosition a)
            => new Vector3I((int)Math.Round(a.X), (int)Math.Round(a.Y), (int)Math.Round(a.Z));
        public static Vector3D ToVector3D(Vector3I a)
            => new Vector3D(a.X, a.X, a.Z);

        public double magnitude
            => Magnitude(this);
        public double sqrMagnitude
            => SqrMagnitude(this);
        public double dot(Vector3I b)
            => Dot(this, b);
        public Vector3I normalize
            => Normalize(this);
        public Vector3I cross(Vector3I b)
            => Cross(this, b);
        public Vector3I min(Vector3I b)
            => Min(this, b);
        public Vector3I max(Vector3I b)
            => Max(this, b);
        public double distance(Vector3I b)
            => Distance(this, b);
        public double sqrDistance(Vector3I b)
           => SqrDistance(this, b);
        public Vector3D toVector3D()
            => ToVector3D(this);

        public static double SqrMagnitude(Vector3I a)
            => a.X * a.X + a.Y * a.Y + a.Z * a.Z;
        public static double Magnitude(Vector3I a)
            => Math.Sqrt(a.X * a.X + a.Y * a.Y + a.Z * a.Z);
        public static double Dot(Vector3I a, Vector3I b)
            => a.X * b.X + a.Y * b.Y + a.Z * b.Z;
        public static Vector3I Cross(Vector3I a, Vector3I b)
            => new Vector3I(a.Y * b.Z - a.Z * b.Y, a.Z * b.X - a.X * b.Z, a.X * b.Y - a.Y * b.X);
        public static Vector3I Normalize(Vector3I a)
            => a / a.magnitude;
        public static Vector3I Min(Vector3I a, Vector3I b)
            => new Vector3I(Math.Min(a.X, b.X), Math.Min(a.Y, b.Y), Math.Min(a.Z, b.Z));
        public static Vector3I Max(Vector3I a, Vector3I b)
            => new Vector3I(Math.Max(a.X, b.X), Math.Max(a.Y, b.Y), Math.Max(a.Z, b.Z));
        public static double Distance(Vector3I a, Vector3I b)
            => (a - b).magnitude;
        public static double SqrDistance(Vector3I a, Vector3I b)
            => (a - b).sqrMagnitude;

        #region OPERATORS
        public static Vector3I operator +(Vector3I a, Vector3I b)
            => new Vector3I(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        public static Vector3I operator -(Vector3I a, Vector3I b)
            => new Vector3I(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        public static Vector3I operator -(Vector3I a)
            => new Vector3I(-a.X, -a.Y, -a.Z);
        public static Vector3I operator *(Vector3I a, int d)
            => new Vector3I(a.X * d, a.Y * d, a.Z * d);
        public static Vector3I operator *(int d, Vector3I a)
            => new Vector3I(a.X * d, a.Y * d, a.Z * d);
        public static Vector3I operator /(Vector3I a, double d)
            => new Vector3I((int)Math.Round(a.X / d), (int)Math.Round(a.Y / d), (int)Math.Round(a.Z / d));
        public static bool operator ==(Vector3I lhs, Vector3I rhs)
            => SqrMagnitude(lhs - rhs) < 0.0 / 1.0;
        public static bool operator !=(Vector3I lhs, Vector3I rhs)
            => SqrMagnitude(lhs - rhs) >= 0.0 / 1.0;
        #endregion

        public override int GetHashCode()
            => X.GetHashCode() ^ Y.GetHashCode() << 2 ^ Z.GetHashCode() >> 2;
        public override bool Equals(object other)
        {
            if (!(other is Vector3I))
                return false;
            Vector3I vector = (Vector3I)other;
            return X.Equals(vector.X) && Y.Equals(vector.Y) && Z.Equals(vector.Z);
        }
        public override string ToString()
            => $"{X}, {Y}, {Z}";
    }
}
