# OQ.VectorHelpers
**A set of helper classes so you don't have to use location/position when working with minebot**

Forcefield example
```
    public class Forcefield : ITask, ITickListener
    {
        public override bool Exec()
        {
            return !Context.Player.IsDead();
        }
        public async Task OnTick()
        {
            var nearest = Context.Entities.GetClosestPlayer();
            if (nearest == null) return;

            //convert nearest player and bot's positions to vectors
            Vector3D nearestVector = Vector3D.FromPosition(nearest.Position);
            Vector3D localVector = Vector3D.FromPosition(Context.Player.GetPosition());

            //get the direction vector from the player towards to bot and invert it
            //normalize vector from 1+ range to 0-1 and do it times 10
            Vector3D backwardsVector = -(nearestVector - localVector).normalize * 10;

            //backwards vector is a direction vector thus we have to add it to the nearest player vector
            Vector3D targetPosition = nearestVector + backwardsVector;

            //now we check if the player is within 10m and if so we move towards the calculated position
            if (nearestVector.distance(localVector) < 10)
                await Context.Player.MoveTo(targetPosition.ToLocation(), new MapOptions { Sprint = true, Look = false }).Task;
        }
    }
```
