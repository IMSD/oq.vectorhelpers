using OQ.MineBot.PluginBase.Classes;
using OQ.MineBot.Protocols.Classes.Base;
using System;

namespace OQ.VectorHelpers
{
    public class Vector3D
    {
        public double X;
        public double Y;
        public double Z;
        public Vector3D(double x = 0.0, double y = 0.0, double z = 0.0)
        {
            X = x;
            Y = y;
            Z = z;
        }
        public IPosition ToPosition()
            => new Position(X, Y, Z);
        /// <summary>
        /// Convert to <see cref="ILocation"/> by rounding X and Z
        /// </summary>
        public ILocation ToLocation()
            => new Location((int)Math.Round(X), (float)Y, (int)Math.Round(Z));
        public static Vector3D FromLocation(ILocation a)
            => new Vector3D(a.X, a.Y, a.Z);
        public static Vector3D FromPosition(IPosition a)
            => new Vector3D(a.X, a.Y, a.Z);
        public static Vector3I ToVector3I(Vector3D a)
            => new Vector3I((int)Math.Round(a.X), (int)Math.Round(a.Y), (int)Math.Round(a.Z));

        public double magnitude
            => Magnitude(this);
        public double sqrMagnitude
            => SqrMagnitude(this);
        public double dot(Vector3D b)
            => Dot(this, b);
        public Vector3D normalize
            => Normalize(this);
        public Vector3D cross(Vector3D b)
            => Cross(this, b);
        public double distance(Vector3D b)
            => Distance(this, b);
        public Vector3D min(Vector3D b)
            => Min(this, b);
        public Vector3D max(Vector3D b)
            => Max(this, b);
        public Vector3I toVector3I()
            => ToVector3I(this);
        public double sqrDistance(Vector3D b)
            => SqrDistance(this, b);

        public static double SqrMagnitude(Vector3D a)
            => a.X * a.X + a.Y * a.Y + a.Z * a.Z;
        public static double Magnitude(Vector3D a)
            => Math.Sqrt(a.X * a.X + a.Y * a.Y + a.Z * a.Z);
        public static double Dot(Vector3D a, Vector3D b)
            => a.X * b.X + a.Y * b.Y + a.Z * b.Z;
        public static Vector3D Cross(Vector3D a, Vector3D b)
            => new Vector3D(a.Y * b.Z - a.Z * b.Y, a.Z * b.X - a.X * b.Z, a.X * b.Y - a.Y * b.X);
        public static Vector3D Normalize(Vector3D a)
            => a / a.magnitude;
        public static Vector3D Min(Vector3D a, Vector3D b)
            => new Vector3D(Math.Min(a.X, b.X), Math.Min(a.Y, b.Y), Math.Min(a.Z, b.Z));
        public static Vector3D Max(Vector3D a, Vector3D b)
            => new Vector3D(Math.Max(a.X, b.X), Math.Max(a.Y, b.Y), Math.Max(a.Z, b.Z));
        public static double Distance(Vector3D a, Vector3D b)
            => (a - b).magnitude;
        public static double SqrDistance(Vector3D a, Vector3D b)
            => (a - b).sqrMagnitude;

        #region OPERATORS
        public static Vector3D operator +(Vector3D a, Vector3D b)
            => new Vector3D(a.X + b.X, a.Y + b.Y, a.Z + b.Z);
        public static Vector3D operator -(Vector3D a, Vector3D b)
            => new Vector3D(a.X - b.X, a.Y - b.Y, a.Z - b.Z);
        public static Vector3D operator -(Vector3D a)
            => new Vector3D(-a.X, -a.Y, -a.Z);
        public static Vector3D operator *(Vector3D a, double d)
            => new Vector3D(a.X * d, a.Y * d, a.Z * d);
        public static Vector3D operator *(double d, Vector3D a)
            => new Vector3D(a.X * d, a.Y * d, a.Z * d);
        public static Vector3D operator /(Vector3D a, double d)
            => new Vector3D(a.X / d, a.Y / d, a.Z / d);
        public static bool operator ==(Vector3D lhs, Vector3D rhs)
            => SqrMagnitude(lhs - rhs) < 0.0 / 1.0;
        public static bool operator !=(Vector3D lhs, Vector3D rhs)
            => SqrMagnitude(lhs - rhs) >= 0.0 / 1.0;

        public static Vector3D operator *(Vector3D a, float d)
            => new Vector3D(a.X * d, a.Y * d, a.Z * d);
        public static Vector3D operator *(float d, Vector3D a)
            => new Vector3D(a.X * d, a.Y * d, a.Z * d);
        public static Vector3D operator /(Vector3D a, float d)
            => new Vector3D(a.X / d, a.Y / d, a.Z / d);
        #endregion

        public override int GetHashCode()
            => X.GetHashCode() ^ Y.GetHashCode() << 2 ^ Z.GetHashCode() >> 2;
        public override bool Equals(object other)
        {
            if (!(other is Vector3D))
                return false;
            Vector3D vector = (Vector3D)other;
            return X.Equals(vector.X) && Y.Equals(vector.Y) && Z.Equals(vector.Z);
        }
        public override string ToString()
            => $"{X.ToString("0.0")}, {Y.ToString("0.0")}, {Z.ToString("0.0")}";
    }
}
